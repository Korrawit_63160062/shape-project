/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.shapeproject2;

/**
 *
 * @author DELL
 */
public class TestShape {

    public static void main(String[] args) {
        Circle c1 = new Circle(1.5);
        Circle c2 = new Circle(4.5);
        Circle c3 = new Circle(5.2);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);

        Shape[] shapeCircle = {c1, c2, c3};
        for (int i = 0; i < shapeCircle.length; i++) {
            System.out.println(shapeCircle[i].getName() + " area : " + shapeCircle[i].calArea());
        }
        
        Square s1 = new Square(4.0);
        Square s2 = new Square(2.0);
        System.out.println(s1);
        System.out.println(s2);
        
        Shape[] shapeSquare = {s1, s2};
        for (int i = 0; i < shapeSquare.length; i++) {
            System.out.println(shapeSquare[i].getName() + " area : " + shapeSquare[i].calArea());
        }
        
        Rectangle r1 = new Rectangle(3, 2);
        Rectangle r2 = new Rectangle(4, 3);
        System.out.println(r1);
        System.out.println(r2);
        
        Shape[] shapeRectangle = {r1, r2};
        for (int i = 0; i < shapeRectangle.length; i++) {
            System.out.println(shapeRectangle[i].getName() + " area : " + shapeRectangle[i].calArea());
        }
    }

}
